import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CollaboratorRankComponent } from './collaborator-rank.component';

describe('CollaboratorRankComponent', () => {
  let component: CollaboratorRankComponent;
  let fixture: ComponentFixture<CollaboratorRankComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CollaboratorRankComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CollaboratorRankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
