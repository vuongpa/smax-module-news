import {Component, OnInit} from '@angular/core';
import {ToastrService} from "ngx-toastr";
import { NewsService } from 'src/app/services/api/news.service';
import { News } from 'src/app/types/viewmodels';
import { NewsStore } from '../news.store';
import { NewsDetailComponent } from '../news-detail/news-detail.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { debounce } from "lodash";

@Component({
  selector: 'app-news-table',
  templateUrl: './news-table.component.html',
  styleUrls: ['./news-table.component.scss']
})
export class NewsTableComponent implements OnInit {
  dataSource$ = this.store.dataSource$;
  limit$ = this.store.limit$;
  total$ = this.store.total$;
  loading$ = this.store.loading$;
  keyword: string = "";

  constructor(
    private readonly newsService: NewsService,
    private readonly toastrService: ToastrService,
    private readonly store: NewsStore,
    private readonly modalService: BsModalService
  ) {}

  ngOnInit() {
    this.store.getTableDataSource({});
  }
  onDelete(news: News) {
    const isAgree = confirm(`Bạn có chắc chắn muốn xóa ${news.title}`)
    if (isAgree) {
      this.newsService.news.delete(news._id).subscribe({
        next: () => {
          this.toastrService.success(`Xóa thành công ${news.title}`)
          this.store.getTableDataSource({});
        },
        error: () => {
          this.toastrService.error(`Xóa không thành công ${news.title}. Vui lòng thử lại`)
        }
      })
    }
  }
  onReload() {
    this.store.getTableDataSource({});
  }
  onPageChange(pageChanged?: any) {
    if (pageChanged) {
      this.store.updateLimit(pageChanged.itemsPerPage);
      this.store.updatePage(pageChanged.page);
    }
    this.store.getTableDataSource({});
  }
  onCreate() {
    this.modalService.show(NewsDetailComponent)
  }
  onEdit(news: News) {
    this.modalService.show(NewsDetailComponent, {
      initialState: { news }
    })
  }
  searchNews = debounce(() => {
    if (this.keyword) {
      this.store.getTableDataSource({q: this.keyword});
    } else {
      this.store.getTableDataSource({});
    }
  }, 300);
}
