import * as mongoose from 'mongoose'

export const requestCreateCategory = {
  title: { type: String, required: true },
  description: { type: String, required: true },
  parentId: { type: mongoose.Schema.Types.ObjectId },
  thumbnail: [String]
}
export const requestGetCategory = {
  title: { type: String },
  description: { type: String },
  parentId: { type: mongoose.Schema.Types.ObjectId },
  thumbnail: [String]
}