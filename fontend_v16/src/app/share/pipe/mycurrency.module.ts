import { NgModule } from '@angular/core';
import { MycurrencyPipe } from './mycurrency.pipe';
import { MaskPipe, NgxMaskModule } from 'ngx-mask';
@NgModule({
  declarations: [MycurrencyPipe],
  imports: [
    NgxMaskModule.forRoot()
  ],
  exports: [
    MycurrencyPipe,
  ],
  providers: [MaskPipe]
})
export class MycurrencyModule { }
