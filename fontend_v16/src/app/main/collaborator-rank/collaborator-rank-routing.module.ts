import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';

import { CollaboratorRankComponent } from './collaborator-rank.component'

export const routes: Routes = [] = [
  {
    path: '',
    component: CollaboratorRankComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CollaboratorRankRoutingModule { }
