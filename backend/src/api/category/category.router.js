import { Router } from 'express'
import categoryController from './category.controller'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { requestCreateCategory, requestGetCategory } from './category.dto'

const router = new Router()

router.get('/',
  query(requestGetCategory),
  categoryController.findMany)

router.get('/:id',
  categoryController.findById)

router.get('/:id/child-level',
  query(requestGetCategory),
  categoryController.findChildLevel)

router.post('/',
  body(requestCreateCategory),
  categoryController.create)

router.put('/:id',
  query(),
  body(requestCreateCategory),
  categoryController.update)

router.delete('/:id',
  query(),
  categoryController.delete)

export default router
