import {Injectable, OnDestroy} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseApiService } from './base.service';
import {Category, EntityResult} from 'src/app/types/viewmodels';
import { environment } from 'src/environments/environment';
import {Subject, takeUntil} from "rxjs";
import {AuthService} from "./auth.service";

@Injectable({
  providedIn: 'root',
})
export class CategoryService extends BaseApiService implements OnDestroy {
  private defaultParams: any = {};
  destroy = new Subject();
  api = { category: 'categories' }
  category = {
    get: (params = {}) =>
      this.httpClient.get<EntityResult<Category[]>>(this.createUrlDev([this.api.category]), {
        params: this.createParams(Object.assign(params, this.defaultParams)),
      }),
    create: (body = {}) =>
      this.httpClient.post<EntityResult<Category>>(this.createUrlDev([this.api.category]), body),
    update: (id: string, body = {}) =>
      this.httpClient.put<EntityResult<Category>>(this.createUrlDev([this.api.category, id]), body),
    show: (id: string) =>
      this.httpClient.get<EntityResult<Category>>(this.createUrlDev([this.api.category, id])),
    delete: (id: string) =>
      this.httpClient.delete(this.createUrlDev([this.api.category, id])),
    getChildLevel: (id: string, params = {}) =>
      this.httpClient.get<EntityResult<Category[]>>(this.createUrlDev([this.api.category, id, 'child-level']), {
        params: this.createParams(Object.assign(params, this.defaultParams)),
      }),
  };

  constructor(
    httpClient: HttpClient,
    private authService: AuthService,
  ) {
    super(httpClient);
    this.authService.currentBiz.pipe(takeUntil(this.destroy)).subscribe({
      next: res => {
        if (res) {
          this.setApiAddress(environment.apiAddress, `bizs/${res.alias}/categories`)
          this.defaultParams = {
            bizId: res.id,
          }
        }
      }
    })
  }

  ngOnDestroy(): void {
    // Called once, before the instance is destroyed.
    // Add 'implements OnDestroy' to the class.
    this.destroy.next(true);
    this.destroy.complete();
  }
}
