export interface ObjectAny {
  [name: string]: any;
}

export interface Option {
  id?: string;
  label?: string;
  platform?: string;
  value?: string | number;
  alias?: string | string[];
  bgColor?: string;
  txtColor?: string;
}

export interface EntityPagination<T> {
  [name: string]: any;
  rows: T[];
  currentRow?: T | null;
  currentRowId?: string | null;
  limit?: number;
  page?: number;
  total?: number;
  search?: string | null;
  debounce?: number;
  isGet?: boolean;
  loading: boolean;
  loadingGetRow?: boolean;
  timeout?: any;
  after?: string | null;
  query?: any;
  sort?: any;
}
// ReturnType<typeof setTimeout> | null

export interface QueryMen {
  // agency?: number
  limit?: number;
  page?: number;
  before?: Date;
  after?: Date;
  q?: string;
}

export interface Pagination {
  total: number;
  page: number;
  limit: number;
}

export interface BackendError {
  messages?: string[];
  silent?: boolean;
}
export interface GetResult<T> {
  entity: T;
}
export interface EntityResult<T> {
  status: number;
  statusText: string;
  subStatus: number;
  subStatusText: string;
  message: string;
  refToken?: string;
  total: number;
  data: T;
  viewer?: User;
}
export interface GetAllResult<T> {
  entity: Array<T>;
}
export interface QueryResult<T> {
  entity: EntityResult<T>;
}
export interface AuthResult {
  token: string;
  user: User;
}
export interface Image {
  url?: string;
  urls?: string[];
  loading?: boolean;
}

export interface User {
  id: string;
  name: string;
  email: string;
  phone?: string;
  province?: string;
  district?: string;
  ward?: string;
  street?: string;
  address?: string;
  picture: string;
  gender?: string;
  birthday?: string;
  role: string;
  biz: Biz;
  quickModules: string[];
  services: {
    facebooks?: string[];
    googles?: string[];
  };
  groupIds?: string[];
  groups?: BizGroup[];
  roleIds?: string[];
  roles?: BizRole[];
  token: string;
  createdAt?: Date;
}
export interface Setting {
  canNewBiz: boolean;
  canSignUp: boolean;
  pageDomains: string;
}
export interface Biz {
  id: string;
  alias: string;
  author?: {
    id: string;
    name: string;
    email: string;
    picture: string;
  };
  name: string;
  email?: string;
  picture?: string;
  isActive: boolean;
  currency: {
    code: string;
    exchangeRates: string[];
    mask: string;
    name: string;
    symbol: string;
    thousandSeparator: string;
  };

  domains: BizDomain[];
  module: BizModule;
  modules: BizModule[];
  quickModules: string[];
  groups: BizGroup[];
  roles: BizRole[];
  user: User;
  users: User[];
  timezone: string;
  createdAt?: Date;
  updatedAt?: Date;
}
export interface BizDomain {
  id: string;
  name: string;
  disabled: boolean;
  bizId: string;
  ssl: string;
  verify: string;
  createdAt: Date;
  updatedAt: Date;
}
export interface BizRole {
  id: string;
  desc: string;
  icon: string;
  isActive: boolean;
  name: string;
}
export interface BizModule {
  id: string;
  name: string;
  alias: string;
  icon: string;
  isActive: true;
  isDefault: false;
  isInside: false;
  weight: number;
  createdAt: Date;
  updatedAt: Date;
}
export interface BizGroup {
  id: string;
  name: string;
  desc: string;
  modules: BizModule[];
  scopeIds: string[];
  isActive: boolean;
  createdAt: Date;
  updatedAt: Date;
}
export interface Category {
  _id: string;
  title: string;
  description?: string;
  thumbnail?: string;
  parentId?: string;
  totalNews?: number;
}
export interface News {
  _id: string;
  title: string;
  description?: string;
  thumbnail?: string;
  categoryId?: string;
  content: string;
  createdAt: Date;
  updatedAt: Date;
}
