export const requestCreateNews = {
  title: { type: String, require: true },
  description: { type: String, require: true },
  thumbnail: { type: String },
  content: { type: String, require: true },
  categoryId: { type: Object, require: true }
}
export const requestGetNews = {
  title: { type: String },
  description: { type: String },
  thumbnail: { type: String },
  content: { type: String },
  categoryId: { type: [String] }
}
