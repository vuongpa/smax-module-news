import { NgModule } from '@angular/core';
import { FilterColorOrderPipe } from './filterColorOrder.pipe';
@NgModule({
  declarations: [FilterColorOrderPipe
  ],
  imports: [
  ],
  exports: [
    FilterColorOrderPipe
  ],
})
export class PipeFilterColorOrderModule { }
