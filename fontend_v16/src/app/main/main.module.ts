import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { HeaderModule } from '../share/header/header.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SidebarModule } from '../share/sidebar/sidebar.module';
import { CategoryModule } from "../category/category.module";
import {NewsModule} from "../news/news.module";
import {CKEditorModule} from "ng2-ckeditor";


@NgModule({
  declarations: [
    MainComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MainRoutingModule,
    HeaderModule,
    SidebarModule,
    CategoryModule,
    NewsModule,
    CKEditorModule
  ]
})
export class MainModule { }
