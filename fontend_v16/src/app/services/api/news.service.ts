import {Injectable, OnDestroy} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseApiService } from './base.service';
import {EntityResult, News} from 'src/app/types/viewmodels';
import { environment } from 'src/environments/environment';
import {Subject, takeUntil} from "rxjs";
import {AuthService} from "./auth.service";

@Injectable({
  providedIn: 'root',
})
export class NewsService extends BaseApiService implements OnDestroy {
  private defaultParams: any = {};
  destroy = new Subject();
  api = {
    news: 'news'
  }
  news = {
    get: (params = {}) =>
      this.httpClient.get<EntityResult<News[]>>(this.createUrlDev([this.api.news]), {
        params: this.createParams(Object.assign(params, this.defaultParams)),
      }),
    create: (body = {}) =>
      this.httpClient.post<EntityResult<News>>(this.createUrlDev([this.api.news]), body),
    update: (id: string, body = {}) =>
      this.httpClient.put<EntityResult<News>>(this.createUrlDev([this.api.news, id]), body),
    show: (id: string) =>
      this.httpClient.get<EntityResult<News>>(this.createUrlDev([this.api.news, id])),
    delete: (id: string) =>
      this.httpClient.delete(this.createUrlDev([this.api.news, id])),
    countByCategory: (params = {}) => this.httpClient.get<EntityResult<any[]>>(this.createUrlDev([this.api.news, "count-by-category"]), {
      params: this.createParams(Object.assign(params, this.defaultParams)),
    }),
  };

  constructor(
    httpClient: HttpClient,
    private authService: AuthService,
  ) {
    super(httpClient);
    this.authService.currentBiz.pipe(takeUntil(this.destroy)).subscribe({
      next: res => {
        if (res) {
          this.setApiAddress(environment.apiAddress, `bizs/${res.alias}/news`)
          this.defaultParams = {
            bizId: res.id,
          }
        }
      }
    })
  }

  ngOnDestroy(): void {
    // Called once, before the instance is destroyed.
    // Add 'implements OnDestroy' to the class.
    this.destroy.next(true);
    this.destroy.complete();
  }
}
