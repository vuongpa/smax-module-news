'use strict'

import { model, Schema } from 'mongoose'

const DOCUMENT_NAME = 'News'
const COLLECTION_NAME = 'News'

export const NewsSchema = new Schema({
  title: { type: String, required: true, index: 1, trim: true },
  thumbnail: { type: String },
  description: { type: String, trim: true },
  content: { type: String, required: true },
  categoryId: { type: Schema.Types.ObjectId, ref: 'Category', index: true, required: true },
  public: { index: true, type: Boolean, default: true },
  active: { index: true, type: Boolean, default: true },
  deletedAt: { type: Date }
}, {
  timestamps: true,
  collection: COLLECTION_NAME
})
NewsSchema.index({ title: 'text' })

export const News = model(DOCUMENT_NAME, NewsSchema)
