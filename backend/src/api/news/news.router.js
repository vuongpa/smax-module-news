import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import newsController from './news.controller'
import { requestCreateNews, requestGetNews } from './news.dto'

const router = new Router()

router.get('/',
  query(requestGetNews),
  newsController.findMany)

router.get('/count-by-category',
  query({ categoryId: [String] }),
  newsController.countByCategory)

router.get('/:id',
  newsController.findById)

router.post('/',
  body(requestCreateNews),
  newsController.create)

router.put('/:id',
  query(),
  body(requestCreateNews),
  newsController.update)

router.delete('/:id',
  query(),
  newsController.delete)

export default router
