import { Injectable } from "@angular/core";
import { Category } from "../types/viewmodels"
import { ComponentStore, tapResponse } from "@ngrx/component-store";
import {Observable, withLatestFrom} from "rxjs";
import { CategoryService } from "../services/api/category.service";
import { NewsService } from "../services/api/news.service";

export interface CategoryState {
  dataSource: Category[];
  loading: {
    table: boolean;
    submit: boolean;
  };
  limit: number;
  page: number;
  total: number;
  documentDetail?: Category;
}

@Injectable({providedIn: 'root'})
export class CategoryStore extends ComponentStore<CategoryState> {
  readonly documentDetail$ = this.select(s => s.dataSource);
  readonly dataSource$ = this.select(s => s.dataSource);
  readonly loading$ = this.select(s => s.loading);
  readonly limit$ = this.select(s => s.limit);
  readonly page$ = this.select(s => s.page);
  readonly total$ = this.select(s => s.total);


  readonly vm$ = this.select(
    this.dataSource$,
    this.documentDetail$,
    this.loading$,
    this.limit$,
    this.total$,
    this.page$,
    (dataSource, documentDetail, loading, limit, total, page) => ({dataSource, documentDetail, loading, limit, total, page})
  )

  constructor(
    private readonly categoryService: CategoryService,
    private readonly newsService: NewsService
  ) {
    super({
      dataSource: [],
      loading: {table: false, submit: false},
      limit: 20,
      page: 1,
      total: 0,
    })
  }

  // Updater
  readonly updateDataSource = this.updater((state, dataSource: Category[]) => ({...state, dataSource}))
  readonly updateLimit = this.updater((state, value: number) => ({...state, limit: value}))
  readonly updatePage = this.updater((state, value: number) => ({...state, page: value}))
  readonly updateTotal = this.updater((state, value: number) => ({...state, total: value}))
  readonly setTableLoading = this.updater((state, loading: boolean) => ({
    ...state,
    loading: {...state.loading, table: loading}
  }))
  readonly setSubmitLoading = this.updater((state, loading: boolean) => ({
    ...state,
    loading: {...state.loading, submit: loading}
  }))

  // Effects
  readonly getTableDataSource = this.effect((query$: Observable<any>) =>
    query$.pipe(
      withLatestFrom(this.limit$, this.page$),
      tapResponse(
        ([query, limit, page]) => {
          this.newsService.news.countByCategory().subscribe({
            next: response => {
              const totalNews = response.data;

              this.categoryService.category.get({limit, page,...query}).subscribe({
                next: res => {
                  this.updateDataSource(res.data.map(category => ({
                    ...category,
                    totalNews: totalNews.find(data => data.categoryId === category._id)?.count || "_"
                  })));
                  this.updateTotal(res.total);
                  this.setTableLoading(false);
                },
                error: () => {
                  this.setTableLoading(false);
                }
              })
            }
          })
        },
        err => console.log(err)
      ),
    ))

  // Functions
}
