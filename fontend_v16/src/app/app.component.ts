import { Component } from '@angular/core';
import { AuthService } from './services/api/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Tin tức';
  constructor(private authService: AuthService) { }
  ngOnInit(): void {
    this.authService.popular();
  }
}
