import { Injectable } from "@angular/core";
import {Category, News, QueryMen} from "../types/viewmodels"
import { ComponentStore, tapResponse } from "@ngrx/component-store";
import { Observable, withLatestFrom } from "rxjs";
import { NewsService } from "../services/api/news.service";

export interface NewsState {
  dataSource: News[];
  loading: {
    table: boolean;
    submit: boolean;
  };
  limit: number;
  page: number;
  total: number;
  documentDetail?: News;
  categories: Category[];
  categoriesFilter: string[];
}

@Injectable({providedIn: 'root'})
export class NewsStore extends ComponentStore<NewsState> {
  readonly documentDetail$ = this.select(s => s.dataSource);
  readonly dataSource$ = this.select(s => s.dataSource);
  readonly loading$ = this.select(s => s.loading);
  readonly limit$ = this.select(s => s.limit);
  readonly page$ = this.select(s => s.page);
  readonly total$ = this.select(s => s.total);
  readonly categories$ = this.select(s => s.categories);
  readonly categoriesFilter$ = this.select(s => s.categoriesFilter);


  readonly vm$ = this.select(
    this.dataSource$,
    this.documentDetail$,
    this.loading$,
    this.limit$,
    this.total$,
    this.page$,
    (dataSource, documentDetail, loading, limit, total, page) => ({dataSource, documentDetail, loading, limit, total, page})
  )

  constructor(
    private readonly newsService: NewsService
  ) {
    super({
      dataSource: [],
      loading: {table: false, submit: false},
      limit: 20,
      page: 1,
      total: 0,
      categories: [],
      categoriesFilter: []
    })
  }

  // Updater
  readonly updateDataSource = this.updater((state, dataSource: News[]) => ({...state, dataSource}))
  readonly updateLimit = this.updater((state, value: number) => ({...state, limit: value}))
  readonly updatePage = this.updater((state, value: number) => ({...state, page: value}))
  readonly updateTotal = this.updater((state, value: number) => ({...state, total: value}))
  readonly updateCategories = this.updater((state, value: Category[]) => ({...state, categories: value}))
  readonly updateCategoriesFilter = this.updater((state, value: string[]) => ({...state, categoriesFilter: value}))
  readonly setTableLoading = this.updater((state, loading: boolean) => ({
    ...state,
    loading: {...state.loading, table: loading}
  }))

  // Effects
  readonly getTableDataSource = this.effect((query$: Observable<any>) =>
    query$.pipe(
      withLatestFrom(this.limit$, this.page$, this.categoriesFilter$),
      tapResponse(
        ([querymen, limit, page, categoryIds]) => {
          let query: any = {limit, page, ...querymen};
          if (categoryIds.length) {
            query.categoryId = categoryIds;
          }
          this.newsService.news.get(query).subscribe({
            next: res => {
              this.updateDataSource(res.data);
              this.updateTotal(res.total);
              this.setTableLoading(false);
            },
            error: () => {
              this.setTableLoading(false);
            }
          })
        },
        err => console.log(err)
      )
    ))

  readonly insertCategoryFilter = this.effect((id$: Observable<string>) => id$.pipe(
    withLatestFrom(this.categoriesFilter$),
    tapResponse(
      ([id, categoriesFilter]) => {
        this.updateCategoriesFilter([...categoriesFilter, id])
      },
      err => {console.log(err)}
    )
  ))
  readonly removeCategoryFilter = this.effect((id$: Observable<string>) => id$.pipe(
    withLatestFrom(this.categoriesFilter$),
    tapResponse(
      ([id, categoriesFilter]) => {
        this.updateCategoriesFilter(categoriesFilter.filter(c => c !== id))
      },
      err => {console.log(err)}
    )
  ))
  // Functions
}
