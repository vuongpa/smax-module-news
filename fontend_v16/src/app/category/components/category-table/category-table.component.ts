import {Component, OnInit} from '@angular/core';
import {Category} from "../../../types/viewmodels";
import {CategoryService} from "../../../services/api/category.service";
import {ToastrService} from "ngx-toastr";
import { CategoryStore } from '../../category.store';
import { CategoryDetailComponent } from '../category-detail/category-detail.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import {debounce} from "lodash";

@Component({
  selector: 'app-category-table',
  templateUrl: './category-table.component.html',
  styleUrls: ['./category-table.component.scss']
})
export class CategoryTableComponent implements OnInit {
  dataSource$ = this.store.dataSource$;
  limit$ = this.store.limit$;
  total$ = this.store.total$;
  loading$ = this.store.loading$;
  keyword: string = "";

  constructor(
    private readonly categoryService: CategoryService,
    private readonly toastrService: ToastrService,
    private readonly store: CategoryStore,
    private readonly modalService: BsModalService
  ) {}

  ngOnInit() {
    this.store.getTableDataSource({});
  }
  onDelete(category: Category) {
    const isAgree = confirm(`Bạn có chắc chắn muốn xóa ${category.title}`)
    if (isAgree) {
      this.categoryService.category.delete(category._id).subscribe({
        next: () => {
          this.toastrService.success(`Xóa thành công ${category.title}`)
          this.store.getTableDataSource({});
        },
        error: () => {
          this.toastrService.error(`Xóa không thành công ${category.title}. Vui lòng thử lại`)
        }
      })
    }
  }
  onReload() {
    this.store.getTableDataSource({});
  }
  onPageChange(pageChanged?: any) {
    if (pageChanged) {
      this.store.updateLimit(pageChanged.itemsPerPage);
      this.store.updatePage(pageChanged.page);
    }
    this.store.getTableDataSource({});
  }
  onCreate() {
    this.modalService.show(CategoryDetailComponent)
  }
  onEdit(category: Category) {
    this.modalService.show(CategoryDetailComponent, {
      initialState: {category}
    })
  }
  searchNews = debounce(() => {
    if (this.keyword) {
      this.store.getTableDataSource({q: this.keyword});
    } else {
      this.store.getTableDataSource({});
    }
  }, 300);
}
