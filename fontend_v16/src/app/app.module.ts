import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorHandler, Injectable, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToastrModule } from 'ngx-toastr';
import { TokenInterceptor } from './services/token.interceptor';
import { APP_BASE_HREF } from '@angular/common';
import {MainModule} from "./main/main.module";
import {CategoryModule} from "./category/category.module";
import {CKEditorModule} from "ng2-ckeditor";
import { StoreModule } from '@ngrx/store';
import { CollapseModule } from 'ngx-bootstrap/collapse';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
  handleError(error: any): void {
    console.error(error);
    const chunkFailedMessage = /Loading chunk [\d]+ failed/;
    if (chunkFailedMessage.test(error.message)) {
      window.location.reload();
    }
  }
}

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    MainModule,
    CategoryModule,
    CKEditorModule,
    StoreModule.forRoot(),
    CollapseModule.forRoot()
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },
    { provide: ErrorHandler, useClass: GlobalErrorHandler },
    { provide: APP_BASE_HREF, useValue: (window as any)['_app_base'] || '/' },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
