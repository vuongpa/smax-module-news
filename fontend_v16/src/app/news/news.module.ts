import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewsComponent } from './news.component';
import {NewsRoutingModule} from "./news-routing.module";
import { InputEditorModule } from '../share/input/input-editor/input-editor.module';
import { NewsTableComponent } from './news-table/news-table.component';
import { NewsDetailComponent } from './news-detail/news-detail.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CategoryRoutingModule } from '../category/category-routing.module';
import { InputUploadModule } from '../share/input/input-upload/input-upload.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { NewsStore } from './news.store';
import { CollapseModule } from 'ngx-bootstrap/collapse';



@NgModule({
  declarations: [
    NewsComponent,
    NewsTableComponent,
    NewsDetailComponent
  ],
  imports: [
    NewsRoutingModule,
    CommonModule,
    FormsModule,
    ModalModule.forRoot(),
    ReactiveFormsModule,
    PaginationModule,
    InputEditorModule,
    InputUploadModule,
    NgSelectModule,
    CollapseModule.forRoot(),
  ],
  providers: [NewsStore]
})
export class NewsModule { }
