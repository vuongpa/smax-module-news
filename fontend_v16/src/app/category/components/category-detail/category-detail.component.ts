import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {Category} from "../../../types/viewmodels";
import { CategoryService } from 'src/app/services/api/category.service';
import { ToastrService } from 'ngx-toastr';
import { CategoryStore } from '../../category.store';

@Component({
  selector: 'app-category-detail',
  templateUrl: './category-detail.component.html',
  styleUrls: ['./category-detail.component.scss']
})
export class CategoryDetailComponent implements OnInit {
  form!: FormGroup;
  loading = {select: false, submit: false};
  @Input() category: Category | null = null
  editorConfig = {
    lang: 'en',
    extraPlugins: 'youtube,colorbutton,justify',
    removeButtons: 'Copy,Cut,Paste,Undo,Redo,Print,PasteText,PasteFromWord',
    height: '150px'
  }
  keyword: string = "";
  parentCategories: Category[] = [];
  childCategories: Category[] = [];

  constructor(
    private readonly fb: FormBuilder,
    private readonly modalRef: BsModalRef,
    private readonly categoryService: CategoryService,
    private readonly toastrService: ToastrService,
    private readonly store: CategoryStore,
    private readonly modalService: BsModalService
  ) {}

  close() {
    this.modalRef.hide()
  }
  ngOnInit() {
    this.getSelectDataSource();
    this.form = this.fb.group(this.generatorForm())
    if (this.category) {
      this.form.patchValue(this.category)
    }
    this.modalRef.setClass('modal-xl')
    if (this.category?._id) {
      this.categoryService.category.getChildLevel(this.category._id).subscribe(res => {
        this.childCategories = res.data;
      })
    }
  }
  onSubmit() {
    const isValidForm = this.form.valid;
    if (!isValidForm) {
      return;
    }

    this.loading.submit = true;
    let action = this.categoryService.category.create(this.form.value);
    if (this.form.value._id) {
      action = this.categoryService.category.update(this.form.value._id, this.form.value);
    }
    action.subscribe({
      next: () => {
        if (this.form.value._id) {
          this.toastrService.success('Cập nhật thành công');
        }
        this.modalRef.hide();
        this.loading.submit = false;
        this.store.getTableDataSource({});
      },
      error: () => {
        this.loading.submit = false;
        this.toastrService.error('Cập nhật không thành công. Vui lòng thử lại');
      }
    });
  }
  generatorForm() {
    return {
      _id: null,
      title: null,
      description: null,
      thumbnail: null,
      parentId: null
    }
  }
  execSelected(ev: any) {
    console.log("ev: ", ev);
  }
  execKeypress(ev: any) {
    console.log("ev: ", ev);
  }
  getSelectDataSource() {
    this.loading.select = true
    this.categoryService.category.get().subscribe({
      next: res => {
        this.parentCategories = res.data;
        this.loading.select = false;
      },
      error: () => {
        this.loading.select = false;
      }
    })
  }
  onEdit(category: Category) {
    this.modalService.show(CategoryDetailComponent, {
      initialState: {category}
    })
  }
}
