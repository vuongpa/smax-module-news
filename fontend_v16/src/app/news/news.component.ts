import { Component, OnInit } from '@angular/core';
import { NewsStore } from './news.store';
import { CategoryService } from '../services/api/category.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  show = true;
  categories$ = this.store.categories$;
  isCheck: boolean = false;

  constructor(
    private readonly store: NewsStore,
    private readonly categoryService: CategoryService
  ) {}

  ngOnInit(): void {
    this.categoryService.category.get().subscribe({
      next: response => {
        this.store.updateCategories(response.data);
      },
      error: error => {
        console.log("load categories error: ", error);
      }
    })
  }
  filterCategory(id: string, ev: any) {
    if (!ev.target.checked) {
      this.store.removeCategoryFilter(id);
    } else {
      this.store.insertCategoryFilter(id);
    }
    this.store.getTableDataSource({});
  }
}
