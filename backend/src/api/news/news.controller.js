import { success } from '../../services/response'
import { News } from './news.model'
import { Types } from 'mongoose'
import { queryWithFullTextSearch } from '../../services/query.service'

class NewsController {
  create = async (req, res, next) => {
    try {
      const category = await News.create(req.bodymen.body)
      success(res, 201)({ data: category })
    } catch (err) {
      next(err)
    }
  }

  findMany = async (req, res, next) => {
    try {
      const { query, select, cursor } = queryWithFullTextSearch(req)
      const [total, categories] = await Promise.all([
        News.countDocuments(query),
        News.find(query, select, cursor)
      ])
      success(res, 200)({ data: categories, total })
    } catch (err) {
      next(err)
    }
  }

  countByCategory = async (req, res, next) => {
    try {
      const categoryId = req.querymen.categoryId
      const query = {
        active: true,
        deletedAt: { $exists: false }
      }
      if (Array.isArray(categoryId)) {
        query.categoryId = { $in: categoryId.map(id => Types.ObjectId(id)) }
      } else if (categoryId) {
        query.categoryId = Types.ObjectId(categoryId)
      }

      const result = await News.aggregate([
        { $match: query },
        {
          $group: {
            _id: '$categoryId',
            count: { $count: { } }
          }
        },
        {
          $project: {
            categoryId: '$_id',
            count: '$count',
            _id: 0
          }
        }
      ])
      success(res, 200)({ data: result })
    } catch (err) {
      next(err)
    }
  }

  findById = async (req, res, next) => {
    try {
      const category = await News.findById(req.params.id)
      success(res, 200)({ data: category })
    } catch (err) {
      next(err)
    }
  }

  update = async (req, res, next) => {
    try {
      await News.findByIdAndUpdate(req.params.id, req.body)
      const category = await News.findById(req.params.id)
      success(res, 200)({ data: category })
    } catch (err) { next(err) }
  }

  delete = async (req, res, next) => {
    try {
      await News.findByIdAndUpdate(req.params.id, { deletedAt: new Date() })
      success(req, 200)({})
    } catch (err) { next(err) }
  }
}

const newsController = new NewsController()
export default newsController
