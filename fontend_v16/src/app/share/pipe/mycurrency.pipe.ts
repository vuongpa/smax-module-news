import { Pipe, PipeTransform } from '@angular/core';
import { MaskPipe } from 'ngx-mask';
import { Subject, takeUntil } from 'rxjs';
import { AuthService } from 'src/app/services/api/auth.service';
@Pipe({ name: 'mycurrency' })
export class MycurrencyPipe implements PipeTransform {
  destroy = new Subject();
  option = {
    mask: 'separator.2',
    symbol: "₫",
    thousandSeparator: ","
  };
  constructor(
    private authService: AuthService,
    private maskPipe: MaskPipe
  ) {
    this.authService.currentBiz.pipe(takeUntil(this.destroy)).subscribe({
      next: res => {
        if (res) {
          this.option = {
            mask: res.currency.mask,
            symbol: res.currency.symbol,
            thousandSeparator: res.currency.thousandSeparator,
          }
        }
      }
    })
  }

  ngOnDestroy(): void {
    this.destroy.next(true);
    this.destroy.complete();
  }
  transform(value: number): string | null {
    return this.maskPipe.transform(value, this.option.mask, this.option.thousandSeparator) + this.option.symbol;
  }
}
