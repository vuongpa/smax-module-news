import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryComponent } from './category.component';
import { CategoryDetailComponent } from './components/category-detail/category-detail.component';
import { CategoryTableComponent } from './components/category-table/category-table.component';
import { CategoryRoutingModule } from "./category-routing.module";
import { FormsModule , ReactiveFormsModule} from "@angular/forms";
import { InputEditorModule } from "../share/input/input-editor/input-editor.module";
import { ModalModule } from "ngx-bootstrap/modal";
import { PaginationModule } from "ngx-bootstrap/pagination";
import { InputUploadModule } from "../share/input/input-upload/input-upload.module";
import { NgSelectModule } from '@ng-select/ng-select';
import { CategoryStore } from './category.store';
import {TabsModule} from "ngx-bootstrap/tabs";



@NgModule({
  declarations: [
    CategoryComponent,
    CategoryDetailComponent,
    CategoryTableComponent
  ],
  imports: [
    CommonModule,
    CategoryRoutingModule,
    FormsModule,
    ModalModule.forRoot(),
    InputEditorModule,
    ReactiveFormsModule,
    PaginationModule,
    InputEditorModule,
    InputUploadModule,
    NgSelectModule,
    TabsModule
  ],
  providers: [CategoryStore]
})
export class CategoryModule { }
