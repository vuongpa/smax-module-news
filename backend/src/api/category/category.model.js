'use strict'

import { Schema, model } from 'mongoose'
import * as mongoose from 'mongoose'

const DOCUMENT_NAME = 'Category'
const COLLECTION_NAME = 'Categories'

export const CategorySchema = new Schema({
  thumbnail: String,
  title: { type: String, required: true, index: true, trim: true },
  description: { type: String, trim: true },
  parentId: { index: true, type: mongoose.Schema.Types.ObjectId, ref: 'Category' },
  public: { index: true, type: Boolean, default: true },
  active: { index: true, type: Boolean, default: true },
  deletedAt: { type: Date }
}, {
  timestamps: true,
  collection: COLLECTION_NAME
})
CategorySchema.index({ title: 'text' })

export const Category = model(DOCUMENT_NAME, CategorySchema)
