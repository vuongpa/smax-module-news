import { Category } from './category.model'
import { success } from '../../services/response'
import { queryWithFullTextSearch } from '../../services/query.service'

class CategoryController {
  create = async (req, res, next) => {
    try {
      const category = await Category.create(req.bodymen.body)
      success(res, 201)({ data: category })
    } catch (err) {
      next(err)
    }
  }

  findMany = async (req, res, next) => {
    try {
      const { query, select, cursor } = queryWithFullTextSearch(req)
      const [total, categories] = await Promise.all([
        Category.countDocuments(query),
        Category.find(query, select, cursor)
      ])
      success(res, 200)({ data: categories, total })
    } catch (err) {
      next(err)
    }
  }

  findChildLevel = async (req, res, next) => {
    try {
      const { querymen: { select, cursor }, params } = req
      const query = { ...req.querymen.query, parentId: params.id }
      const [total, categories] = await Promise.all([
        Category.countDocuments(query),
        Category.find(query, select, cursor)
      ])
      success(res, 200)({ data: categories, total })
    } catch (err) {
      next(err)
    }
  }

  findById = async (req, res, next) => {
    try {
      const category = await Category.findById(req.params.id)
      success(res, 200)({ data: category })
    } catch (err) {
      next(err)
    }
  }

  update = async (req, res, next) => {
    try {
      await Category.findByIdAndUpdate(req.params.id, req.body)
      const category = await Category.findById(req.params.id)
      success(res, 200)({ data: category })
    } catch (err) { next(err) }
  }

  delete = async (req, res, next) => {
    try {
      const category = await Category.findByIdAndUpdate(req.params.id, { deletedAt: new Date() })
      success(res, 200)({ data: category })
    } catch (err) { next(err) }
  }
}

const categoryController = new CategoryController()
export default categoryController
