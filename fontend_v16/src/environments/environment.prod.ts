export const environment = {
  production: true,
  apiAddress: 'https://smax.app/api',
  module: 'prepaid-card',
  apiModule: 'https://smax.app/api'
};

const parsedURL = new URL(location.href)
if (parsedURL.hostname !== 'localhost') {
  const serviceAddr = `${ parsedURL.origin }/api`;
  environment.apiAddress = serviceAddr;
  environment.apiModule = serviceAddr;
}
