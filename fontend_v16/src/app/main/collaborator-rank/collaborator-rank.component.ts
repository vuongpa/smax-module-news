import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { MainService } from 'src/app/services/api/main.service';
import { CollaboratorRank } from 'src/app/types/viewmodels';

@Component({
  selector: 'app-collaborator-rank',
  templateUrl: './collaborator-rank.component.html',
  styleUrls: ['./collaborator-rank.component.scss']
})
export class CollaboratorRankComponent implements OnInit {

  @ViewChild('itemModal') itemModal!: ModalDirective;
  items: CollaboratorRank[] = [];


  loading = {
    table: false,
    submit: false,
  }
  form!: FormGroup;


  formSubmitAttempt = false;
  constructor(
    private fb: FormBuilder,
    private mainService: MainService,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    this.getItems();
  }

  getItems() {
    this.loading.table = true;
    this.mainService.collaboratorRank.get({}).subscribe({
      next: res => {
        this.items = res.data;
        this.loading.table = false;
      },
      error: err => {
        this.loading.table = false;
      }
    })
  }


  onSubmit() {
    this.formSubmitAttempt = true;
    if (this.form.valid) {
      this.loading.submit = true;
      const action = this.form.value.id
        ? this.mainService.collaboratorRank.update(this.form.value.id, this.form.value)
        : this.mainService.collaboratorRank.create(this.form.value);

      action.subscribe({
        next: res => {
          console.log('res', res);
          if (this.form.value.id) {
            this.toastr.success(`Cập nhật thành công!`)
            const hasItem = this.items.find(i => i.id === res.data.id);
            if (hasItem) {
              Object.assign(hasItem, res.data);
            }
          } else {
            this.toastr.success(`Thêm ${this.form.value.name} thành công!`);
            this.items.unshift(res.data);
          }
          this.loading.submit = false;
          this.formSubmitAttempt = false;
          this.itemModal.hide();
        },
        error: err => {
          this.loading.submit = false;
          this.formSubmitAttempt = false;
        }
      })
    }
  }
  delItem(item: CollaboratorRank) {
    if (confirm(`Bạn có chắc muốn xóa ${item.name}?`)) {
      this.mainService.collaboratorRank.delete(item.id).subscribe({
        next: res => {
          this.toastr.success(`Xóa ${item.name} thành công!`)
          this.items = this.items.filter(i => i.id !== item.id);
        }
      })
    }
  }
  showModal(item: CollaboratorRank | null = null) {
    this.form = this.fb.group(this.generatorForm());
    if (item) {
      this.form.patchValue(item);
    }
    this.itemModal.show();

  }
  generatorForm() {
    return {
      id: null,
      name: [null, Validators.required],
      desc: null,
      isActive: false,
      amountSales: 0,
      gift: this.fb.group({
        discountType: 'PERCENT',
        discount: 0,
        productPriceCollaborator: null
      }),
      icon: null,
    }
  }

}
