import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {BsModalRef, ModalDirective} from "ngx-bootstrap/modal";
import { ToastrService } from 'ngx-toastr';
import { CategoryService } from 'src/app/services/api/category.service';
import { NewsService } from 'src/app/services/api/news.service';
import { Category, News } from 'src/app/types/viewmodels';
import { NewsStore } from '../news.store';

@Component({
  selector: 'app-news-detail',
  templateUrl: './news-detail.component.html',
})
export class NewsDetailComponent implements OnInit {
  @ViewChild('newsDetailModal') newsDetailModal!: ModalDirective;
  form!: FormGroup;
  loading = {select: false, submit: false};
  @Input() news: News | null = null
  editorConfig = {
    lang: 'en',
    extraPlugins: 'youtube,colorbutton,justify',
    removeButtons: 'Copy,Cut,Paste,Undo,Redo,Print,PasteText,PasteFromWord',
    height: '150px'
  }
  keyword: string = "";
  categories$ = this.store.categories$;

  constructor(
    private readonly fb: FormBuilder,
    private readonly modalRef: BsModalRef,
    private readonly toastrService: ToastrService,
    private readonly newsService: NewsService,
    private readonly store: NewsStore
  ) {}

  close() {
    this.modalRef.hide()
  }
  ngOnInit() {
    this.form = this.fb.group(this.generatorForm())
    if (this.news) {
      this.form.patchValue(this.news as any)
    }
    this.modalRef.setClass('modal-xl')
  }
  onSubmit() {
    const isValidForm = this.form.valid;
    if (!isValidForm) {
      return;
    }

    this.loading.submit = true;
    let action = this.newsService.news.create(this.form.value);
    if (this.form.value._id) {
      action = this.newsService.news.update(this.form.value._id, this.form.value);
    }
    action.subscribe({
      next: () => {
        if (this.form.value._id) {
          this.toastrService.success('Cập nhật thành công');
        }
        this.modalRef.hide();
        this.loading.submit = false;
        this.store.getTableDataSource({});
      },
      error: () => {
        this.loading.submit = false;
        this.toastrService.error('Cập nhật không thành công. Vui lòng thử lại');
      }
    });
  }
  generatorForm() {
    return {
      _id: null,
      title: null,
      description: null,
      thumbnail: null,
      categoryId: null,
      content: null
    }
  }
  execSelected(ev: any) {
    console.log("ev: ", ev);
  }
  execKeypress(ev: any) {
    console.log("ev: ", ev);
  }
}
