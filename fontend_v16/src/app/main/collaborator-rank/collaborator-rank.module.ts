import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CollaboratorRankComponent } from './collaborator-rank.component';
import { CollaboratorRankRoutingModule } from './collaborator-rank-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

import { ModalModule } from 'ngx-bootstrap/modal';
import { MycurrencyModule } from 'src/app/share/pipe/mycurrency.module';
import { CurrencyMaskInputMode, NgxCurrencyModule } from 'ngx-currency';
import { InputEditorModule } from 'src/app/share/input/input-editor/input-editor.module';

const customCurrencyMaskConfig = {
  align: "left",
  allowNegative: false, // ko nhập giá trị âm
  allowZero: true,
  decimal: ".",
  precision: 0,
  prefix: "",
  suffix: "đ",
  thousands: ",",
  nullable: true,
  inputMode: CurrencyMaskInputMode.FINANCIAL
};
@NgModule({
  declarations: [
    CollaboratorRankComponent
  ],
  imports: [
    CommonModule,
    CollaboratorRankRoutingModule,
    MycurrencyModule,
    InputEditorModule,
    NgxCurrencyModule.forRoot(customCurrencyMaskConfig),
    ModalModule.forRoot(),
    ReactiveFormsModule,
  ]
})
export class CollaboratorRankModule { }
