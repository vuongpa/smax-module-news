import axios from 'axios';
import { apiEndpoint } from '../../config'
// import { serviceToken } from "../../config"

// axios.interceptors.request.use(function (config) {
//   config.headers.Authorization = `Bearer ${serviceToken}`;
//   return config;
// })

function createUrl(paths) {
  return apiEndpoint + '/' + paths.join('/');
}

export const $get = (paths, queryParams) => {
  return axios.get(createUrl(paths), queryParams)
    .then(res => res.data)
}

export const $post = (paths, body, config) => {
  console.log(paths, body, config);
  return axios.post(createUrl(paths), body, config)
    .then(res => res.data)
}
