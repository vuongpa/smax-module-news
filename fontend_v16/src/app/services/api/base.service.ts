import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class BaseApiService {
  private apiAddress = environment.apiAddress;

  constructor(protected httpClient: HttpClient) { }

  protected createParams(params: { [key: string]: any }): HttpParams {
    return Object.keys(params).reduce((m, k) => {
      if (params[k] != null) {
        return m.set(k, params[k].toString());
      }
      return m;
    }, new HttpParams());
  }

  protected createUrl(paths: string[]) {
    let api = this.apiAddress + '/' + paths.join('/');
    return api.replace(/\/+$/, '');
  }

  protected createUrlDev(paths: string[]) {
    let api = "http://localhost:6900/api" + '/' + paths.join('/');
    return api.replace(/\/+$/, '');
  }

  public setApiAddress(apiAddress: string, endpoint: string) {
    this.apiAddress = apiAddress + '/' + endpoint;
    this.apiAddress = this.apiAddress.replace(/\/+$/, '');
  }
}
