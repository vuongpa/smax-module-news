export function queryWithFullTextSearch (req) {
  const querymen = getBaseQuery(req)
  const q = req.query.q

  if (querymen && q) {
    let query = querymen.query || {}
    let cursor = querymen.cursor || {}
    let select = querymen.select || {}
    delete query.keywords

    query = { ...query, $text: { $search: q } }
    cursor = { ...cursor, sort: { ...cursor.sort, score: { $meta: 'textScore' } } }
    select = { ...select, score: { $meta: 'textScore' } }

    return { query, select, cursor }
  }

  return querymen
}

export function getBaseQuery (req) {
  const querymen = req.querymen
  const query = querymen.query || {}
  return { ...querymen, query: { ...query, deletedAt: { $exists: false } } }
}
